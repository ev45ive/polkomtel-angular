git clone https://bitbucket.org/ev45ive/polkomtel-angular.git

cd polkomtel-angular
npm install
npm run start
<!-- lub -->
ng serve -o 

# Ankieta
https://tiny.pl/7rmjm

# Instalacje
node -v 
npm -v  
code -v 
git --version

# Cwiczenia
git stash
git pull -f


# Angular CLI
npm i -g @angular/cli

# Generowanie projektu
cd ..
ng new polkomtel-angular

? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS   
[ https://sass-lang.com/documentation/syntax#scss ]

# Uruchomienie 
cd polkomtel-angular
npm run start

# Proxy
~/.gitconfig
~/.npmrc
git config --global http.proxy http://proxyuser:proxypwd@WASZ_SERWER_PROXYTUTAJ:8080 

# VsCode 
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2
https://marketplace.visualstudio.com/items?itemName=johnpapa.angular-essentials
https://marketplace.visualstudio.com/items?itemName=infinity1207.angular2-switcher
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

# Emmet
https://docs.emmet.io/cheat-sheet/

# Chrome extenstion
https://augury.rangle.io/

# Generators
ng g m core --routing -m app 
ng g m shared -m app

ng g m shared -m app --force

## Features
ng g m playlists --routing -m app

ng g c playlists/views/playlists
ng g c playlists/components/playlist-list
ng g c playlists/components/playlist-list-item
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form

ng g p shared/yesno --export 

# Bootstrap
npm install bootstrap

angular.json:
```
            "styles": [
              "node_modules/bootstrap/dist/bootstrap.css",
```

# Chokidar Ubuntu
It’s hitting your system's file watchers limit

Try echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

# MusicSearch
ng g m --help
ng g c --help

ng g m music-search --routing -m app

ng g c music-search/views/albums-search

ng g c music-search/components/search-form
ng g c music-search/components/search-results
ng g c music-search/components/album-card

ng g s music-search/service/albums-search 


# Auth
ng g m core/security -m app
ng g s core/security/auth 


# RxJS
https://rxjs-dev.firebaseapp.com/ (open console F12)

https://rxjs-dev.firebaseapp.com/operator-decision-tree
https://rxmarbles.com/#interval
https://rxviz.com/examples/pause-and-resume

# Http Interecptors
ng g interceptor core/security/auth

# Keycodes / Shortcuts
https://www.npmjs.com/package/angular2-hotkeys