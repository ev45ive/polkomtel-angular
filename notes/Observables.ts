type Observer = { next: Function, error: Function, complete: Function }

type TeardownFn = () => void

type SubscibeFn = (observer: Observer) => TeardownFn

interface Observable {
  constructor(subscriber: SubscibeFn);
  // constructor(subscriber: (observer: { next: Function, error: Function, complete: Function }) => () => void);
}

const o = new Observable((observer)=>{
  const handle = setTimeout(()=>{
    observer.next(123)
    this.next(234)
    this.next(345)
    observer.complete()
  },2000)
  return () => { clearTimeout(handle)}
})

o.subscribe({
  next: console.log,
  complete: console.log
})
.unsubscribe()


/* =========== */
const o = new rxjs.Observable((obs) => {
  var i = 0;
  var h = setInterval(()=>{
      console.log('Internal '+i)
      obs.next(i++)
      if(i > 5){ obs.complete() }
  },1000)

  return () => {
      console.log('cleardown')
      clearInterval(h)
  }
})

const sub = o.subscribe({
  next: v => console.log(1,v), 
  complete: () => console.log('complete 1') 
})
const = o.subscribe({
  next: v => console.log(2,v), 
  complete: () => console.log('complete 2') 
})

// setTimeout(()=>{ sub.unsubscribe() },3000)

/* ================== */

/* Swtiching to new observable inside subscription */

o = new rxjs.Observable((obs) => {
  var i = 0;
  var h = setInterval(()=>{
      obs.next(i++)
      if(i > 0){ obs.complete() }
  },3000)

  return () => {
      console.log('cleardown')
      clearInterval(h)
  }
})

valueChanges = new rxjs.Subject()
sub = new rxjs.Subscription()

valueChanges.subscribe( v => {
  if(sub){ sub.unsubscribe() }
  console.log('new validation')
  
  sub = o.subscribe( n => {
      console.log('new result')
  })
})

// setTimeout(()=>{ sub.unsubscribe() },3000)