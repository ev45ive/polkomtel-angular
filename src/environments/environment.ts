// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { AuthConfig } from 'src/app/core/security/auth.service';

// ng build --env=demo environment.demo.ts
// ng build --env=test environment.test.ts
// ng build --env=swieta environment.swieta.ts

export const environment = {
  production: false,

  search_api_url: 'https://api.spotify.com/v1/search',

  authConfig: {
    auth_url:'https://accounts.spotify.com/authorize',
    client_id: 'b36345e45a2244798757f31602125c5d',
    redirect_uri: 'http://localhost:4200/search',
    response_type: 'token',
    scopes: [],
    show_dialog: 'true',
    state: ''
  } as AuthConfig
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
