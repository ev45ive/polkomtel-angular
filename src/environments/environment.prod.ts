import { AuthConfig } from 'src/app/core/security/auth.service';

export const environment = {
  production: true,
  
  search_api_url: 'https://api.spotify.com/v1/search',

  authConfig: {
    auth_url:'https://accounts.spotify.com/authorize',
    client_id: 'b36345e45a2244798757f31602125c5d',
    redirect_uri: 'http://localhost:4200/search',
    response_type: 'token',
    scopes: [],
    show_dialog: 'true',
    state: ''
  } as AuthConfig
};
