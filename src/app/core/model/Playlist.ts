
/*  */

interface Entity {
  id: number;
  name: string;
}

export interface Playlist extends Entity {
  /**
   * Is playlist public?
   */
  public: boolean;
  description: string;
  // tracks: [string, Track]
  // tracks: Array<Track>
  tracks?: Track[]
}

interface Track extends Entity { }

const data: Playlist = {
  id: 123,
  name: 'test',
  public: true,
  description: 'desc',
  // tracks: []
}

// data.tracks && data.tracks.length
// data.tracks.length


// if (typeof data.id == 'string') {
//   data.id.concat()
// } else {
//   data.id.toExponential()
// }


// interface Point { x: number, y: number }
// interface Vector { x: number, y: number, length: 123 }

// let p: Point = { x: 123, y: 234 }
// let v: Vector = { x: 123, y: 234, length: 123 }

// p = v
// v = p


export type PlaylistFormDTO = Pick<Playlist, 'name' | 'public' | 'description'>

// type Draft = Readonly<Partial<Pick<Playlist, 'name' | 'public' | 'description'>>>

// type Partial<T> = {
//   [k in keyof T]?: T[k]
// }

// type PlaylistDraft = {
//   [k in 'name'|'public'] : string
// }

// type PlaylistDraft = {
//   name:     Playlist['name']
//   public:     Playlist['public'];
//   description: Playlist['description'];
// }
