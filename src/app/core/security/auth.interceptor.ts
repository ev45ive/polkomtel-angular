import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private auth: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // if(!req.url.contains('spotify.api.com/..')){ return next.handle(req)}

    const authReq = req.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.auth.getToken()
      }
    })

    return next.handle(authReq).pipe(
      catchError((error, originalSourceObservable) => {
        if (!(error instanceof HttpErrorResponse)) {
          return throwError(error)
        }

        if (error.status === 401) {
          setTimeout(() => this.auth.authorize(), 2000)
        }

        return throwError(new Error(error.error.error.message))
      })
    )
  }
}

// A.next = B 
// B.next = C
// C.next = Server

// result = A.handle(req)