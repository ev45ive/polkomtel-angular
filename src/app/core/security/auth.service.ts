import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpParams } from '@angular/common/http';

export abstract class AuthConfig {
  auth_url!: string
  client_id!: string
  response_type = 'token' // 'token' 'code'
  redirect_uri!: string
  state = ''
  // A space-separated list of scopes: see Using Scopes.
  scopes: string[] = []
  show_dialog = 'true'
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: any;
  init() {
    this.extractToken()
  }

  constructor(private config: AuthConfig) {

  }

  authorize() {
    sessionStorage.removeItem('token')
    const { client_id, state, show_dialog, scopes, response_type, redirect_uri, auth_url } = this.config

    const p = new HttpParams({
      fromObject: {
        client_id, state, show_dialog, response_type,
        redirect_uri, scope: scopes.join(' ')
      }
    })

    window.location.href = (`${auth_url}?${p.toString()}`)
  }

  extractToken() {
    if (!this.token) {
      const t = (sessionStorage.getItem('token'))
      this.token = t && JSON.parse(t)
    }

    if (!this.token && window.location.hash) {
      const p = new HttpParams({
        fromString: window.location.hash
      })
      this.token = p.get('#access_token')
      sessionStorage.setItem('token', JSON.stringify(this.token))
      window.location.hash = ''
    }

    if (!this.token) {
      this.authorize()
    }
  }

  getToken() {
    return this.token
  }

}
