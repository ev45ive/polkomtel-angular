import { NgModule, ModuleWithProviders, Provider } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthConfig, AuthService } from './auth.service';
import { environment } from 'src/environments/environment';
import { HTTP_INTERCEPTORS, HttpClientXsrfModule } from '@angular/common/http';
import { AuthInterceptor } from './auth.interceptor';


type SecurityModuleOptions = {
  /**
   * Config params oAuth2
   */
  config: AuthConfig;
  /**
   * Auto include headers
   */
  tokenUrlPatterns: string[];
};

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientXsrfModule.disable(),
    // HttpClientXsrfModule.withOptions({
    //   cookieName:'token',
    //   headerName:'Authorization'
    // })
  ],
  providers: [
    // {
    //   provide: AuthConfig,
    //   useValue: environment.authConfig
    // },
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: AuthInterceptor,
    //   multi:true
    // },
  ]
})
export class SecurityModule {

  constructor(private auth: AuthService) {
    this.auth.init()
  }

  static forRoot({ config }: SecurityModuleOptions): ModuleWithProviders {


    return {
      ngModule: SecurityModule,
      // Has to be static for analysis at copile time
      providers: [
        {
          provide: AuthConfig,
          useValue: config
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true
        }
      ]
    }
  }
}
