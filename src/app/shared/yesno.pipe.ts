import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'yesno',
  // pure: false// disbles cache
})
export class YesnoPipe implements PipeTransform {

  transform(value: boolean, yes = 'Yes', no = 'No') {
    return value ? yes : no;
  }

}
