import { Directive, ElementRef, Input } from '@angular/core';
import { NgModel, Validator, AbstractControl, ValidationErrors, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appTestValidator]',
  providers: [
    {
      provide: NG_VALIDATORS, 
      multi: true, 
      useExisting: TestValidatorDirective
    }
  ]
})
export class TestValidatorDirective implements Validator {

  @Input('appTestValidator')
  word = 'test'

  constructor(
    private elemRef: ElementRef,
    // private model: NgModel
  ) {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return (''+control.value).includes(this.word) ? {
      test: {
        word: this.word
      },
      // required: true
    } : null
  }

  ngOnInit() {

  }

}
