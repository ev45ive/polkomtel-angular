import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YesnoPipe } from './yesno.pipe';

import { FormsModule, NgModel } from '@angular/forms';
import { TestValidatorDirective } from './test-validator.directive';
import { CardComponent } from './card/card.component';
import { FieldErrorsComponent } from './field-errors/field-errors.component'

// NgModel

@NgModule({
  declarations: [YesnoPipe, TestValidatorDirective, CardComponent, FieldErrorsComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [YesnoPipe, FormsModule, TestValidatorDirective, CardComponent, FieldErrorsComponent]
})
export class SharedModule { }
