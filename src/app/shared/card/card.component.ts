import { Component, OnInit, ContentChild, AfterContentInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, AfterContentInit {

  @ContentChild('closeCardRef')
  closeCardRef: any

  ngAfterContentInit(){
    this.closeCardRef
  }

  ngAfterViewInit(){}

  constructor() { }

  ngOnInit(): void {
  }

  open(){}

  close(){}
  
}
