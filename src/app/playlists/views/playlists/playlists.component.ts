import { Component, OnInit } from '@angular/core';
import { NgIf } from '@angular/common';
import { Playlist } from 'src/app/core/model/Playlist';

type Modes = 'show' | 'edit'

NgIf

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent implements OnInit {

  mode: Modes = 'show'

  selected?: Playlist

  playlists: Playlist[] = [
    {
      id: 123,
      name: 'Placki 123',
      public: false,
      description: 'Lubie placki'
    },
    {
      id: 234,
      name: 'Placki 234',
      public: false,
      description: 'Lubie placki'
    }, {
      id: 345,
      name: 'Placki 345',
      public: false,
      description: 'Lubie placki'
    }
  ]

  selectPlaylistById(selectedId: Playlist['id']) {
    this.selected = this.playlists.find(p => p.id == selectedId)
  }

  removePlaylist(selectedId: Playlist['id']) {
    console.log('Remove ' + selectedId)
  }

  constructor() {
    this.selectPlaylistById(123)
  }

  ngOnInit(): void {
  }

  editMode() {
    this.mode = 'edit'
  }

  showMode() {
    this.mode = 'show'
  }

  savePlaylist(draft: Playlist) {
    // POST /playlist/123 
    // GET /playlists
    
    const index = this.playlists.findIndex(p => p.id === draft.id)
    this.playlists.splice(index, 1, draft)
    this.selectPlaylistById(draft.id)
    this.showMode()
  }

}
