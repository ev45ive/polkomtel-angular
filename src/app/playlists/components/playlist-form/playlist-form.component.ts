import { Component, OnInit, Input, EventEmitter, Output, ViewChild, AfterViewInit } from '@angular/core';
import { Playlist, PlaylistFormDTO } from 'src/app/core/model/Playlist';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-playlist-form',
  templateUrl: './playlist-form.component.html',
  styleUrls: ['./playlist-form.component.scss']
})
export class PlaylistFormComponent implements OnInit, AfterViewInit {

  @Input() playlist!: Playlist

  @Output() cancel = new EventEmitter()
  @Output() save = new EventEmitter<Playlist>()

  sendCancel() {
    this.cancel.emit(/* $event in parent */)
  }

  constructor() {
  }

  // @ViewChild(NgForm, { read: NgForm, static: true })
  @ViewChild('formRef', { read: NgForm, static: true })
  formRef!: NgForm;

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    // setTimeout(() => {
      // console.log(this.formRef)
      // this.formRef.setValue()
      // this.formRef.getError('required')
      // this.formRef.disabled()
    // })
  }


  sendSave(formDTO: PlaylistFormDTO) {
    if (this.formRef.invalid) { return }

    const { description, name } = formDTO
    const isPublic = formDTO.public

    const draft: Playlist = {
      ...this.playlist,
      description, name,
      public: isPublic
    }

    this.save.emit(draft)
  }
}
