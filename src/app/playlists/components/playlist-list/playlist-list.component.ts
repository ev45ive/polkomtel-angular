import { Component, OnInit, ViewEncapsulation, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { NgForOf, NgForOfContext } from '@angular/common';
import { Playlist } from 'src/app/core/model/Playlist';

NgForOf
NgForOfContext

@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss'],
  // encapsulation:ViewEncapsulation.Emulated
  // inputs:['items:playlists']
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistListComponent implements OnInit {

  @Input('items') playlists: Playlist[] = []

  @Input()
  selected?: Playlist['id']

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(playlist: Playlist) {
      this.selectedChange.emit(playlist)
  }

  @Output()
  removed = new EventEmitter<Playlist>()

  remove(playlist: Playlist, event:MouseEvent) {
    event.stopPropagation()
    this.removed.emit(playlist)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
