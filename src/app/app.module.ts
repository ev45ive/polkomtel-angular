import { BrowserModule } from '@angular/platform-browser';
import { NgModule, DoBootstrap, ApplicationRef, Inject } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { PlaylistsModule } from './playlists/playlists.module';
import { PlaygroundModule } from './playground/playground.module';
import { SecurityModule } from './core/security/security.module';

import { HTTP_INTERCEPTORS, HttpInterceptor, HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    SharedModule,
    PlaylistsModule,
    PlaygroundModule,
    // MusicSearchModule,
    HttpClientModule,
    SecurityModule.forRoot({
      config: environment.authConfig,
      tokenUrlPatterns: [/* api.spotify.com... */]
    }),
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent/* ,HeaderComponent, FooterComponent */]
})
export class AppModule {
  constructor(@Inject(HTTP_INTERCEPTORS) private inter: HttpInterceptor[]) {
    console.log(inter)
  }
}

// export class AppModule implements DoBootstrap {

//   ngDoBootstrap(appRef: ApplicationRef) {
//     // requestServerConfig().then( ...
//     // appRef.bootstrap(AppComponent,'app-root')
//   }
// }
