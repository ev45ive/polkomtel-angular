import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TabsExamplesComponent } from './views/tabs-examples/tabs-examples.component';


const routes: Routes = [
  {
    path: 'playground',
    component: TabsExamplesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaygroundRoutingModule { }
