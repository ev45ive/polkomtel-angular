import { Component, OnInit } from '@angular/core';
import { TabComponent } from '../tab/tab.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  toggle(activeTab: TabComponent) {
    this.tabsList.forEach(tab => {
      tab.opened = tab == activeTab? !tab.opened : false
    })
  }

  tabsList: TabComponent[] = []

  constructor() { }

  ngOnInit(): void {
  }

}
