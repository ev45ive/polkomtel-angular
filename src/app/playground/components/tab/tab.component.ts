import { Component, OnInit, Attribute, Input } from '@angular/core';
import { TabsComponent } from '../tabs/tabs.component';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  @Input() title?: string

  opened = false

  toggle(){
    // this.opened = !this.opened
    this.tabs.toggle(this)
  }

  constructor(
    // @Attribute('title') public title: string
    private tabs:TabsComponent
  ) { 
    this.tabs.tabsList.push(this)
  }

  ngOnInit(): void {
  }

}
