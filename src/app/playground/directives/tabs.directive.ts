import { Directive, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { TabDirective } from './tab.directive';

@Directive({
  selector: '[appTabs]'
})
export class TabsDirective implements AfterContentInit {

  @ContentChildren(TabDirective)
  tabsList: QueryList<TabDirective> = new QueryList()

  constructor() { }

  ngAfterContentInit() {
    // console.log(this.tabsList)
    this.tabsList.forEach(activeTab => {
      activeTab.opened.subscribe((isOpen:boolean) => {
        this.toggle(activeTab, isOpen);
      })
    })
  }


  private toggle(activeTab: TabDirective, isOpen: boolean) {
    this.tabsList.forEach(tab => {
      if(tab === activeTab){
        tab.toggle(!tab.active)
      }else{
        tab.toggle(false)
      }
    });
  }
}
