import { Directive, ElementRef, Renderer2, HostBinding } from '@angular/core';
import { TabDirective } from './tab.directive';

@Directive({
  selector: '[appTabContent]',
  // host:{
  //   '[class.show]':'open',
  //   '(click)':'activagte()'
  // }
})
export class TabContentDirective {

  @HostBinding('class.show')
  get open(){
    return this.tab.active
  }

  constructor(
    private elem: ElementRef<HTMLElement>,
    private renderer: Renderer2,
    private tab: TabDirective
  ) {

    // elem.nativeElement.classList.add('show')
    // renderer.addClass(elem.nativeElement, 'show')

  }

  // When @Input s changes
  ngOnChanges() { }

  ngOnInit() { }

  ngDoCheck() {
    // debugger
    // this.open = this.tab.active
    // this.renderer.addClass(elem.nativeElement, 'show')
  }

  ngOnDestroy() { }

}
