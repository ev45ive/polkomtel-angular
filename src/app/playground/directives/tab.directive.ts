import { Directive, Output, EventEmitter, ViewContainerRef, ContentChild, TemplateRef, EmbeddedViewRef } from '@angular/core';
import { TabContentTplDirective } from './tab-content-tpl.directive';

type TabContentContext = {
  $implicit: TabDirective,
  placki?: 123
}

@Directive({
  selector: '[appTab]'
})
export class TabDirective {

  headerClicked() {
    this.opened.emit(this.active)
  }

  active = false

  toggle(active: boolean) {
    this.active = active;

    if (this.active && this.contentContainer.length === 0) {
      this.contentContainer.createEmbeddedView(this.contentTpl, {
        $implicit: this
      })
    } else {
      // delete this.viewRef
      this.contentContainer.clear()
    }
  }

  // viewRef?: EmbeddedViewRef<TabContentContext>

  update() {

  }

  @ContentChild(TabContentTplDirective, { read: TemplateRef })
  contentTpl!: TemplateRef<TabContentContext>

  @ContentChild(TabContentTplDirective, { read: ViewContainerRef })
  contentContainer!: ViewContainerRef

  // @Output()
  public opened = new EventEmitter<boolean>()

  // constructor(private vcr: ViewContainerRef) { }

  ngAfterContentInit() {
    // this.vcr.createEmbeddedView(this.contentTpl.tpl)

  }

}
