import { Directive, Renderer2, ElementRef, Inject, HostListener } from '@angular/core';
import { TabDirective } from './tab.directive';

@Directive({
  selector: '[appTabHeader]'
})
export class TabHeaderDirective {

  constructor(
    private elem: ElementRef<HTMLElement>,
    private renderer: Renderer2,

    @Inject(TabDirective)
    private tab: TabDirective
  ) {
    // renderer.listen(elem.nativeElement,'click',this.toggle)
  }

  @HostListener('click', ['$event.target'])
  toggle(elem: any) {
    this.tab.headerClicked()
  }


}

