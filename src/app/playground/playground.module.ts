import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaygroundRoutingModule } from './playground-routing.module';
import { TabsExamplesComponent } from './views/tabs-examples/tabs-examples.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { TabComponent } from './components/tab/tab.component';
import { TabsDirective } from './directives/tabs.directive';
import { TabDirective } from './directives/tab.directive';
import { TabHeaderDirective } from './directives/tab-header.directive';
import { TabContentDirective } from './directives/tab-content.directive';
import { TabContentTplDirective } from './directives/tab-content-tpl.directive';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [TabsExamplesComponent, TabsComponent, TabComponent, TabsDirective, TabDirective, TabHeaderDirective, TabContentDirective, TabContentTplDirective],
  imports: [
    CommonModule,
    PlaygroundRoutingModule,
    NgbModule 
  ]
})
export class PlaygroundModule { }
