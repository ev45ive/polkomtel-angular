import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'playlists',
    pathMatch: 'full'
  },
  {
    path:'music',
    loadChildren(){
      return import('./music-search/music-search.module').then(
        m => m.MusicSearchModule
      )
    }
  },
  {
    path: '**',
    // component: PageNotFoundComponent
    redirectTo: 'playlists',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // useHash: true,
    enableTracing: true,
    // onSameUrlNavigation:'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
