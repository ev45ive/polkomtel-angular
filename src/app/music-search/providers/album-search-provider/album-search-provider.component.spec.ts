import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumSearchProviderComponent } from './album-search-provider.component';

describe('AlbumSearchProviderComponent', () => {
  let component: AlbumSearchProviderComponent;
  let fixture: ComponentFixture<AlbumSearchProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumSearchProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumSearchProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
