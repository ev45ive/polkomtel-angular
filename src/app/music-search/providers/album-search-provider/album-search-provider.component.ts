import { Component, OnInit } from '@angular/core';
import { AlbumsSearchService } from '../../service/albums-search.service';

@Component({
  selector: 'app-album-search-provider',
  templateUrl: './album-search-provider.component.html',
  // viewProviders: []
  providers: [ // Content components, This component, View comp.
    AlbumsSearchService
  ]
})
export class AlbumSearchProviderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
