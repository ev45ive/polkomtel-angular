import { Directive, SkipSelf } from '@angular/core';
import { AlbumsSearchService } from '../service/albums-search.service';

@Directive({
  selector: '[appAlbumSearchProvider]',
  providers: [ // Content components, This component, View comp.
    AlbumsSearchService
  ]
})
export class AlbumSearchProviderDirective {

  constructor(
    private service: AlbumsSearchService,
    @SkipSelf() private parent:AlbumsSearchService
  ) { }

}
