import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumsSearchComponent } from './views/albums-search/albums-search.component';
import { SyncSearchComponent } from './views/sync-search/sync-search.component';
import { AlbumDetailsComponent } from './views/album-details/album-details.component';


const routes: Routes = [
  {
    path: 'search',
    component: AlbumsSearchComponent
  },
  {
    //    ['/','album',album.id]
    path: 'album/:album_id',
    component: AlbumDetailsComponent
  },
  {
    path: 'sync',
    component: SyncSearchComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicSearchRoutingModule { }
