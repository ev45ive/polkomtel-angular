import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { AlbumsSearchComponent } from './views/albums-search/albums-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { environment } from 'src/environments/environment';
import { SEARCH_URL } from "./service/tokens";
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { SyncSearchComponent } from './views/sync-search/sync-search.component';
import { AlbumSearchProviderComponent } from './providers/album-search-provider/album-search-provider.component';
import { AlbumSearchProviderDirective } from './providers/album-search-provider.directive';
import { AlbumDetailsComponent } from './views/album-details/album-details.component';
import { AlbumsSearchService } from './service/albums-search.service';


@NgModule({
  declarations: [
    AlbumsSearchComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent,
    SyncSearchComponent,
    AlbumSearchProviderComponent,
    AlbumSearchProviderDirective,
    AlbumDetailsComponent
  ],
  imports: [
    CommonModule,
    MusicSearchRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    AlbumsSearchService,
    // {
    //   provide: HttpHandler, useClass: MyBetterHttpHandler 
    // },
    {
      provide: 'placki', useValue: 'placki'
    },
    {
      provide: SEARCH_URL,
      useValue: environment.search_api_url
    },
    // {
    //   provide: AlbumsSearchService,
    //   useFactory(url: string, placki: string) {
    //     return new AlbumsSearchService(url)
    //   },
    //   deps: [SEARCH_URL, 'placki']
    // },
    // {
    //   provide: AlbumsSearchService,
    //   useClass: SpotifyAlbumsSearchService,
    //   // deps: [MOCK_TEST_SEARCH_URL]
    // },
    // {
    //   provide:AlbumsSearchService,
    //   useClass:AlbumsSearchService
    // },
    // AlbumsSearchService,

    // Interfaces are REMOVED in compilation
    // DOESNT WORK
    // {
    //   provide: ISearchService,
    //   useClass: ImplAlbumsSearchService
    // }
  ]
})
export class MusicSearchModule { }
