import { TestBed } from '@angular/core/testing';

import { AlbumsSearchService } from './albums-search.service';

describe('AlbumsSearchService', () => {
  let service: AlbumsSearchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AlbumsSearchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
