import { Injectable, Inject, EventEmitter } from '@angular/core';
import { Album, Image, AlbumsSearchResult } from 'src/app/core/model/Album';
import { SEARCH_URL } from './tokens';
// import { MusicSearchModule } from '../music-search.module';

import { HttpClient } from '@angular/common/http'

import { map, startWith, mergeAll, mergeMap, concatMap, switchAll, switchMap, exhaust, exhaustMap, catchError, distinctUntilChanged, skip, filter } from 'rxjs/operators'
import { of, Subject, concat, merge, ReplaySubject, BehaviorSubject, EMPTY } from 'rxjs';
// import { MusicSearchModule } from '../music-search.module';

@Injectable({
  // providedIn: MusicSearchModule,
  providedIn: 'root',
})
export class AlbumsSearchService {

  resultsChanges = new BehaviorSubject<Album[]>([])
  private queryChanges = new BehaviorSubject<string>('')

  public queries = this.queryChanges.asObservable()

  constructor(
    private http: HttpClient,
    @Inject(SEARCH_URL) private search_api_url: string
  ) {
    (window as any).subject = (this.resultsChanges)

    this.queryChanges.pipe(
      skip(1),
      filter(q => q !== ''),
      map(query => ({ type: 'album', query })),
      switchMap(params => this.makeSearchRequest(params)))
      .subscribe(this.resultsChanges)
  }


  search(query = 'batman') {
    this.queryChanges.next(query)
  }

  getResults() {
    return this.resultsChanges
  }

  makeSearchRequest(params: { type: string; query: string; }) {
    return this.http.get<AlbumsSearchResult>(this.search_api_url, {
      params
    }).pipe(
      catchError(error => {
        return EMPTY
      }),
      map(resp => resp.albums.items))
  }

  fetchAlbumById(id:string) {
    return this.http.get<Album>(`https://api.spotify.com/v1/albums/${id}`)
  }
}