import { InjectionToken } from '@angular/core';
const tokens = {};

export const SEARCH_URL = new InjectionToken<string>(//
  'SEARCH_URL for Music Search Service');
