import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators, ValidatorFn, AbstractControl, ValidationErrors, AsyncValidatorFn } from '@angular/forms';
import { filter, distinctUntilChanged, tap, debounceTime } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit, OnChanges {

  @Output('searchQueryChange')
  searchEmitter = new EventEmitter<string>();

  @Input('searchQuery')
  searchQuery: string = ''

  // @Input('searchQuery')
  // set searchQuery(query:string){
  //   this.searchForm.get('query')?.setValue(query)
  // }

  ngOnChanges(changes: SimpleChanges): void {
    const searchQuery = changes['searchQuery'];
    (this.searchForm.get('query') as FormControl).setValue(searchQuery.currentValue, {
      emitEvent: false,
      // onlySelf:true
    })

    // if (searchQuery.previousValue !== searchQuery.currentValue) {
    // }
  }

  // formDefinition=[{name:'',labell:'type', ....}]

  censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const badword = 'batman'

    if ('string' !== typeof control.value) { throw 'Not as string' }
    const hasError = control.value.includes(badword)

    return hasError ? {
      censor: { badword }
    } : null
  }

  asyncCensor: AsyncValidatorFn = (control: AbstractControl): Observable<ValidationErrors | null> => {
    // return this.http.get(/* validate?p={value} */).pipe(map(resp => resp.errors || null))
    // console.log('FOrm value change - validating')

    return new Observable((observer) => {
      // console.log('Subscribed')

      const handle = setTimeout(() => {
        const result = this.censor(control)
        // console.log('Next result', result)
        observer.next(result)
        observer.complete()
      }, 2000)

      // Teardown logic
      return () => {
        // console.log('Unsubscribed')
        clearTimeout(handle)
      }
    })
    //.subscribe({complete:console.log}).unsubscribe() // Switch 
  }

  searchForm = this.bob.group({
    query: ['', [
      // Validators.pattern('^(batman)'),
      Validators.required,
      Validators.minLength(3),
      // this.censor 
    ], [
        this.asyncCensor
      ]],
    // query3: new FormControl('',{
    //   validators:[],
    //   updateOn:'',
    //   asyncValidators:[]
    // }),
    extra: this.bob.group({
      type: ['album'],
      markets: this.bob.array([
        this.bob.group({
          code: ['PL'],
        })
      ])
    })
  }, [/* FormGroup Validators */])

  markets = this.searchForm.get('extra.markets') as FormArray


  constructor(private bob: FormBuilder) {

    // service.formDataChanges.subscribe(data => form.setValue(data))
    // form.valueChanges.subscribe(formDat => service.setFormData(formdat))

    (window as any).form = this.searchForm
  }


  ngOnInit(): void {
    this.searchForm.get('query')?.statusChanges

    // this.searchForm.get('query')?.valueChanges.subscribe(console.log)
    // this.searchForm.get('query')?.valueChanges.subscribe(console.log)
    // this.searchForm.get('query')?.valueChanges.subscribe(console.log)

    this.searchForm.get('query')?.valueChanges.pipe(
      // tap(console.log),
      // not to many not too fast
      debounceTime(400),
      // not too short >=3
      filter(q => q.length >= 3),
      // no duplictes
      distinctUntilChanged(),
      // ).subscribe(q => console.log(q))
      // ).subscribe(this.searchEmitter) 
    ).subscribe(q => this.searchEmitter.emit(q))
  }

  startSearch(query: string) {
    this.searchEmitter.emit(query)
  }

  addMarket() {
    if (this.markets instanceof FormArray) {
      this.markets.push(this.bob.group({
        code: this.bob.control(''),
      }))
    }
  }

  removeMarket(i: number) {
    this.markets.removeAt(i)
  }

}
