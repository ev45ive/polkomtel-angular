import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Album } from 'src/app/core/model/Album';
import { Router } from '@angular/router';

@Component({
  selector: 'app-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss']
})
export class AlbumCardComponent implements OnInit {

  @Input()
  album!: Album

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  @Output() select = new EventEmitter<Album>();

  selectAlbum() {
    this.router.navigate(['album', this.album.id])
  }

}
