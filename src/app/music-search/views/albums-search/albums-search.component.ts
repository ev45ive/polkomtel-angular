import { Component, OnInit } from '@angular/core';
import { AlbumsSearchService } from '../../service/albums-search.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-albums-search',
  templateUrl: './albums-search.component.html',
  styleUrls: ['./albums-search.component.scss'],
  viewProviders: [
    // {provide: SearchService, useClass: ArtistsSearchService},
    // AlbumsSearchService
  ]
})
export class AlbumsSearchComponent implements OnInit {

  message = ''

  queryChanges = this.service.queries
  resultsChanges = this.service.getResults()


  constructor(
    private service: AlbumsSearchService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParamMap => {
      const query = queryParamMap.get('query')
      if (query) {
        this.service.search(query)
      }
    })
  }

  searchAlbums(query: string) {
    // this.router.navigate(['/search'], {
    // this.router.navigate(['child'], {
    this.router.navigate([], {
      queryParams: {
        query
      },
      replaceUrl:true,
      relativeTo: this.route
    })

    this.service.search(query)
  }
}
