import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, filter, mergeAll, switchAll, switchMap, multicast, refCount, share, shareReplay } from 'rxjs/operators';
import { AlbumsSearchService } from '../../service/albums-search.service';
import { Album, Track } from 'src/app/core/model/Album';
import { Subject, ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss']
})
export class AlbumDetailsComponent implements OnInit {

  album$ = this.route.paramMap.pipe(
    map(paramMap => paramMap.get('album_id')),
    filter(id => id != null),
    switchMap(id => this.service.fetchAlbumById(id!)),
    // multicast(new ReplaySubject()),
    // refCount()
    // share()
    shareReplay()
  )

  constructor(private route: ActivatedRoute,
    private service: AlbumsSearchService) {
  }

  ngOnInit(): void {

  }

  @ViewChild('playerRef', {
    // static: false,
    // read: ElementRef
  })
  playerRef!: ElementRef<HTMLAudioElement>
  volume = 0.2;

  currentTrack?: Track

  play(track: Track) {
    this.currentTrack = track;
    setTimeout(() => {
      this.playerRef.nativeElement?.play()
    }, 0)
  }

}
